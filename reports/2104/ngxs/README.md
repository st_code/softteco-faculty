# NGXS

The meetup discusses the concepts to NGXS as an Angular state manager.

## Resources

Repository [demo](https://github.com/falizyer/api-news-ci)  
Recorded [video](https://youtu.be/tjTCz1TyTh0)  
Google [slides](https://docs.google.com/presentation/d/1Ub6TYYJ3p4AKP9KdRLAj4oTgITF15UOIjzGpEvwatw4/edit?usp=sharing)

## Useful links
* [NGXS](https://www.ngxs.io/)
* [NGRX](https://ngrx.io/)
* [NGXS vs NGRX](https://blog.singular.uk/why-i-prefer-ngxs-over-ngrx-df727cd868b5)
* [NGXS vs NGRX vs Akita (YouTube)](https://www.youtube.com/watch?v=BtVHm0FJIRA)
