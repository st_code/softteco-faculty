# April

## Table of Contents

- [Code style and linting](code_style_and_linting) by Igor Maslakov
- [NGXS](ngxs) by Dmitry Grachev    
- [E2E testing](e2e_testing) by Alexander Dikusard
- [RxJs Observable](rxjs_observable) by Alexander Gvozd
