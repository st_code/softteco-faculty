# Code style and linting

Editor config + Prettier + ESLint [playground](https://github.com/maslakoff/codestyle-playground)  
Recorded [video](https://www.youtube.com/watch?v=sQ6P1ayBueM)


[Editorconfig](https://editorconfig.org/) helps maintain consistent coding styles for multiple developers working on the same project across various editors and IDEs.  
[Prettier](https://prettier.io/) - formatting tool integrating with most editors.  
[ESLint](https://eslint.org/) - ESLint is a tool for identifying and reporting on patterns found in ECMAScript/JavaScript code, with the goal of making code more consistent and avoiding bugs.
