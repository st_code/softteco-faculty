# E2E testing

the testing pyramid and the features of E2E testing were considered.   
Practical examples use CodeceptJS.


Recorded [video](https://youtu.be/B5EkxxsjR6k)  
Presentation [slides](https://drive.google.com/file/d/1LfJCg4vrxwmjOBOiTrjpa5JS2nzPYIMf/view?usp=sharing)  

## Useful links
* [CodeceptJS](https://codecept.io/)
* [The Future of End to End Testing](https://medium.com/@davert/javascript-the-future-of-end-to-end-testing-bfc00e23110b) (article was written by a CodeceptJS author)
* [E2E автоматизация веб-приложений 2k19: выбери свой инструмент!](https://medium.com/@themillenialscrolls/e2e-%D0%B0%D0%B2%D1%82%D0%BE%D0%BC%D0%B0%D1%82%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F-%D0%B2%D0%B5%D0%B1-%D0%BF%D1%80%D0%B8%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D0%B9-2k19-%D0%B2%D1%8B%D0%B1%D0%B5%D1%80%D0%B8-%D1%81%D0%B2%D0%BE%D0%B9-%D0%B8%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BC%D0%B5%D0%BD%D1%82-9624bbafc7e9)
