# RxJS Observable

RxJS Observable subscription/unsubscription flow.   
Memory leaks inspection using Chrome DevTools.


Recorded [video](https://www.youtube.com/watch?v=0sa7q62cMdo)  
Presentation [slides](https://drive.google.com/file/d/1ZMhS562qAeyTM4jWLvLPprljagQR0BAP/view?usp=sharing) 



## Useful links
[Rx Visualizer](https://rxviz.com) Animated playground for Rx Observables  
[RxJS Marbles](https://rxmarbles.com) Interactive diagrams of Rx Observables  
