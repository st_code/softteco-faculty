# Developer onboarding

Overview of typical problems that new developers face during onboarding on the project and ways to solve the problems.

Presentation [slides](https://drive.google.com/file/d/1geOBl0RMxBkTHtN-Pe57mSIqvslyNm43/view)  
Recorded [video](https://youtu.be/aLgVGpUAMbk)

## Additional links
[Gitlab onboarding](https://about.gitlab.com/handbook/people-group/general-onboarding/) - a handbook of Gitlab's people onboarding
