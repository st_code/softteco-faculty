# Gatsby JS

What is Gatsby JS. Why it's great. Pros and cons using in real project.
Comparison and use cases where Gatsby can be used.
Issues when building and deploying static site.

Presentation [slides](https://drive.google.com/file/d/1TB-MoX3GpGJNeGMZyYWIKsI__UNjihYF/view?usp=sharing)
Recorded [video](https://youtu.be/r1RxRaIkZn4)

## Additional links
[Gatsby docs](https://www.gatsbyjs.com/docs/)
[Gatsby plugins](https://www.gatsbyjs.com/plugins/)
[Github project](https://github.com/gatsbyjs/gatsby)
