# Code style and linting

Automate your development process with continuous integration. Get the speed and reliability you need when building, testing, and deploying code. Build software your way using custom job orchestration with workflows.   
The concept of CI / CD is considered using the example of CircleCI + CRA + S3 at the meetup.


[CircleCI + CRA + S3](https://github.com/maslakoff/circleci-cra-s3) an example of deploy process for React application to a S3 bucket using CircleCI.  
Recorded [video](https://youtu.be/mbO0aiOjh1s)

## Additional links
[CircleCI orbs](https://circleci.com/orbs/) a reusable package of YAML configuration.  
[Slack integration](https://github.com/CircleCI-Public/slack-orb/wiki/Setup) the Slack Orb is an "application" that executes as a part of your job on CircleCI. In order to receive notifications, we must authenticate our application.  
[Hosting a static website using S3](https://docs.aws.amazon.com/AmazonS3/latest/userguide/WebsiteHosting.html) a set of instruction how to use S3 as a static website hosting.  
[Blue/green deployment](https://aws.amazon.com/ru/quickstart/architecture/blue-green-deployment/) this Quick Start automatically deploys a blue/green architecture on AWS using AWS CodePipeline.  
