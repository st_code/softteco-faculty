# GraphQL

GraphQL: an introduction meetup  
GraphQL is a strongly typed Api query language that provides an alternative to REST.

Presentation [slides](https://drive.google.com/file/d/1tdgNQmwiDFRU2QWK_OuDyUpdMltvYFGa/view?usp=sharing)  
Recorded [video](https://youtu.be/JBa7Tbcpd8g)

## Additional links  
[GraphQL docs](https://graphql.org/learn/queries/) - Learn about GraphQL, how it works, and how to use it  
[The Apollo Data Graph Platform](https://www.apollographql.com) - Apollo is a platform for building a data graph, a communication layer that seamlessly connects your application clients (such as React and iOS apps) to your back-end services.  
[An example API](https://anilist.gitbook.io/anilist-apiv2-docs) - Free for non-commercial use  
