# SoftTeco Faculty


## Table of Contents
### April'21  
- [Code style and linting](reports/2104/code_style_and_linting) by Igor Maslakov  
- [NGXS](reports/2104/ngxs) by Dmitry Grachev    
- [E2E testing](reports/2104/e2e_testing) by Alexander Dikusar
- [RxJs Observable](reports/2104/rxjs_observable) by Alexander Gvozd    

### May'21  
- [CI/CD with CircleCI](reports/2105/circleci) by Igor Maslakov 
- [GraphQL](reports/2105/graphql) by Dmitry Grachev 
- [Gatsby JS](reports/2105/gatsbyjs) by Nikita Konan


### June'21  
- [Developer onboarding](reports/2106/onboarding) by Egor Kozlov 
